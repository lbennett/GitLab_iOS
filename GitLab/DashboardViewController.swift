//
//  DashboardViewController.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/4/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation
import UIKit
import AeroGearHttp
import AeroGearOAuth2

class DashboardViewController:UITableViewController {
    
    var projects = [NSDictionary]()
    let cellIdentifier = "projectcell"
    var currentlySelectedIndex = 0
    
    override func viewDidLoad() {
        tableView.delegate = self
        print("view did load \(Url.projects)")
        AuthManager.sharedInstance.http.request(.GET, path: Url.projects) { (response, error:NSError?) in
            if error != nil {
                AlertHelper.Alert("Network Error", message: "Unable to get a list of projects. Check your network connection.", button: "Ok", viewController: self)
                return
            }
            self.projects = response as! [NSDictionary]
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        print(projects.count)
        if projects.count > 0 {
            TableViewHelper.EmptyBackgroundView(self)
            return 1
        } else {
            TableViewHelper.EmptyMessage("You don't have any projects yet.\nYou can create up to 10.", viewController: self)
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier)
        }
        
        cell!.textLabel?.text = projects[indexPath.row]["name"]! as? String
        return cell!
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(projects.count)
        return projects.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        currentlySelectedIndex = indexPath.row
        performSegueWithIdentifier("project", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let projectViewController = segue.destinationViewController as! ProjectViewController
        projectViewController.project = projects[currentlySelectedIndex]
    }
    
    @IBAction func addProjectClicked(sender: AnyObject) {
        print("add project")
    }
}

