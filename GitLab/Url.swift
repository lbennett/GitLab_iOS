//
//  Url.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/4/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation

class Url {
    static var BASE_URL = "http://localhost:3000"
    static var BASE_API:String {
        get {
            return "\(self.BASE_URL)/api/v3"
        }
    }
    static var user:String {
        get {
            return "\(self.BASE_API)/user"
        }
    }
    
    static var projects:String {
        get {
            return "\(self.BASE_API)/projects"
        }
    }
    
    static func issuesForProject(projectID:Int) -> String {
        return "\(self.BASE_API)/projects/\(projectID)/issues"
    }
}