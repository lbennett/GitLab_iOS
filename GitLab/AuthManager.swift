//
//  AuthManager.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/4/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation
import AeroGearHttp
import AeroGearOAuth2

enum VisibilityLevel:Int {
    case PUBLIC = 20
    case INTERNAL = 10
    case PRIVATE = 0
}

class AuthManager {
    static let sharedInstance = AuthManager()
    var http:Http!
    var module:OAuth2Module!
}