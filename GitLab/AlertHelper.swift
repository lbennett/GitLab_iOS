//
//  AlertHelper.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/5/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation
import UIKit

class AlertHelper {
    
    class func Alert(title: String, message:String, button: String, viewController:UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.Default, handler: nil))
        viewController.presentViewController(alert, animated: true, completion: nil)
    }
    
}