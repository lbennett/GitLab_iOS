//
//  GitLabViewProtocol.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/5/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation

protocol GitLabViewProtocol {
    var projectID:Int { get set }
    
}