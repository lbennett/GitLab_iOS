//
//  IssuesTableViewController.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/5/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation
import UIKit

class IssuesTableViewController:UITableViewController, GitLabViewProtocol {
    
    var projectID = 0
    let cellIdentifier = "issuecell"
    var issues = [NSDictionary]()
    var currentIssueIndex = 0
    
    override func viewDidLoad() {
        
        self.title = "Issues"
        
        loadIssues();
        
    }
    
    func loadIssues() {
        
        AuthManager.sharedInstance.http.request(.GET, path: Url.issuesForProject(projectID)) { (response, error) in
            if error != nil {
                AlertHelper.Alert("Network Error", message: "Unable to get issues due to a network error. Please try again.", button: "OK", viewController: self)
                return
            }
            
            self.issues = response as! [NSDictionary]
            self.tableView.reloadData()
        }
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        currentIssueIndex = indexPath.row
        performSegueWithIdentifier("issue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let issueViewController = segue.destinationViewController as! IssueViewController
        issueViewController.projectID = projectID
        issueViewController.issue = issues[currentIssueIndex]
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier)
        }
        
        cell!.textLabel?.text = issues[indexPath.row]["title"]! as? String
        return cell!
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return issues.count
    }
    
}