//
//  ProjectViewController.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/4/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import Foundation
import UIKit

class ProjectViewController:UIViewController {
    
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var mergeRequestsButton: UIButton!
    @IBOutlet weak var issuesButton: UIButton!
    @IBOutlet weak var readmeButton: UIButton!
    @IBOutlet weak var visibilityIcon: UIImageView!
    @IBOutlet weak var forkCountLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    var project:NSDictionary?
    var buttons = [UIButton]()
    
    override func viewDidLoad() {
        self.title = project!["name"] as? String
        buttons = [codeButton, mergeRequestsButton, issuesButton, readmeButton]
        
        addAvatar()
        addDescription()
        addStarForkCount()
        configureVisibilityIcon()
        setupButtons()
    }
    
    func addDescription() {
        descriptionLabel.text = project!["description"] as? String
    }
    
    func addStarForkCount() {
        starCountLabel.text = String(project!["star_count"] as! Int)
        forkCountLabel.text = String(project!["forks_count"] as! Int)
    }
    
    func configureVisibilityIcon() {
        switch project!["visibility_level"] as! Int {
        case VisibilityLevel.PRIVATE.rawValue:
            visibilityIcon.image = UIImage(named: "private")
        case VisibilityLevel.INTERNAL.rawValue:
            visibilityIcon.image = UIImage(named: "internal")
        default:
            visibilityIcon.image = UIImage(named: "public")
        }
    }
    
    func setupButtons() {
        for button in buttons {
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor(red: 0.862, green: 0.862, blue: 0.862, alpha: 1.0).CGColor
            button.layer.backgroundColor = UIColor(red: 0.980, green: 0.980, blue: 0.980, alpha: 1.0).CGColor
            button.layer.cornerRadius = 3.0
        }
    }
    
    func addAvatar() {
        let url = self.project!["avatar_url"] as? String
        
        if url == nil {
            self.avatar.image = UIImage(named: "circle")
        } else {
            AuthManager.sharedInstance.http.request(.GET, path: url!, completionHandler: { (response, error) in
                if(error == nil) {
                    self.avatar.image = UIImage(named: "circle")
                } else {
                    print("response \(response)")
                }
            })
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var nextViewController:GitLabViewProtocol
        nextViewController = segue.destinationViewController as! IssuesTableViewController
        nextViewController.projectID = project!["id"] as! Int
    }
    
    @IBAction func readmeClicked(sender: AnyObject) {
        
    }
    
    @IBAction func issueButtonClicked(sender: AnyObject) {
        performSegueWithIdentifier("issues", sender: self)
    }
    
    @IBAction func mergeRequestsButtonClicked(sender: AnyObject) {
        
    }
    
    @IBAction func codeButtonClicked(sender: AnyObject) {
    }
    
    @IBAction func addProjectClicked(sender: AnyObject) {
        print("add project")
    }
}