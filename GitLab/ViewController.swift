//
//  ViewController.swift
//  GitLab
//
//  Created by Jacob Schatz on 6/2/16.
//  Copyright © 2016 Jacob Schatz. All rights reserved.
//

import UIKit
import AeroGearOAuth2
import AeroGearHttp

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func loginClicked(sender: AnyObject) {
        let bundleString = NSBundle.mainBundle().bundleIdentifier
        AuthManager.sharedInstance.http = Http()
        let c = Config(base: Url.BASE_URL,
                       authzEndpoint: "oauth/authorize",
                       redirectURL: "\(bundleString!)://oauth2Callback",
                       accessTokenEndpoint: "oauth/token",
                       clientId: "1b5b882203ca60e4424e90ddfb67af9ff6d6c88b8c86751b93e342666b650964",
                       clientSecret: "f8b8f8b73618b2af2ba1d6e196da8ed39d88bd76b6dd4c65ed5d2469891d83db")
        AuthManager.sharedInstance.module = OAuth2Module(config: c)
        AuthManager.sharedInstance.http.authzModule = AuthManager.sharedInstance.module
        AuthManager.sharedInstance.module.requestAccess { (response:AnyObject?, error:NSError?) in
            if error != nil {
                AlertHelper.Alert("Authentication Error", message: "Cannot sign you in with these credentials", button: "OK", viewController: self)
                return
            } else {
                print("can sign in")
            }
            self.performSegueWithIdentifier("dashboard", sender: self)
            AuthManager.sharedInstance.http.request(.GET, path: Url.user, completionHandler: { (userResponse, error:NSError?) in
                self.performSegueWithIdentifier("dashboard", sender: self)
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

